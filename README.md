# Project 4: Brevet time calculator with Ajax

Author:Bryce Di Geronimo
Email:bdigeron@uoregon.edu
Repo:https://bitbucket.org/bdigeron/proj4-brevets/src/master/
Description: Reimplement the RUSA ACP controle time calculator with Flask and AJAX.

The table below illustrates the Maximum speeds(open times) and the closing times(minimum times). If the control lands within the range, the time is control/(min or max) speed. The hour is rounded down to the nearest integer and the minutes are the remaining fraction without the hours * 60 rounded up. If the control lands on the end of the range such as 200, then it would be 200/15 or 200/34. Also, controls cannot be 20% over the brevet-distance. The 1000-1300 range would never be used for an ACP brevet. Units are in km/hr. 

Control location (km)	Minimum Speed (km/hr)	Maximum Speed (km/hr)
0-200                           15                      34
200-400                         15                      32
400-600                         15                      30 
600-1000                        11.428                  28
1000-1300                       13.333                  26

************
Special cases:
a) If control entered is a negative number, > 1000, or is 20% greater than brevet distance, an invaid date error will appear in the open and close times. 
b) If control is greater than brevet distance and less than 20% greater than brevet distance, control is set to brevet distance 
c) if control = brevet distance, then open and close times will be set to distance/max and distance/min resspectively.

Cases not accounted for:
a) If the user enters a control that is less than the previous control, there is currently no error handling
