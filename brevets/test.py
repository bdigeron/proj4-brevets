import nose 
import acp_times
import arrow

date = "2017-01-01 00:00"
date = arrow.get(date, 'YYYY-MM-DD HH:mm')

#Edge case where control = 0
def test_Zero():
	assert acp_times.open_time(0, 200, date.isoformat()) == "2017-01-01T00:00:00+00:00"

#Open time where control=BrevetDistance
def test_controlEqualsBrevetDistance():
	assert acp_times.open_time(200, 200, date.isoformat()) == "2017-01-01T05:53:00+00:00"

#Close time where control > Brevet Distance
def test_controlGreatThanBrevetDistance():
	assert acp_times.close_time(405, 400, date.isoformat()) == "2017-01-02T02:40:00+00:00"

#standard use case for open time
def test_standardOpenTime():
	assert acp_times.open_time(175, 200, date.isoformat()) == "2017-01-01T05:09:00+00:00"

def test_standardCloseTime():
	assert acp_times.close_time(890, 1000, date.isoformat()) == "2017-01-03T17:23:00+00:00"


# nose.main()


